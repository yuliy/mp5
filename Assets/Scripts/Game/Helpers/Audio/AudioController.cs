﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class AudioClipWrapper
{
    public AudioClip clip;
    public bool UseDelay = false;
    public float delay = 0.0f;
    
    /// <summary>
    /// Handles playing the AudioClip on our AudioClipWrapper given an AudioSource, will handle playing with 
    /// delay depending on the wrapper settings.
    /// </summary>
    public void Play(AudioSource source)
    {
        if (clip == null) return;
        
        source.clip = clip;
        
        if (UseDelay)
        {
            source.PlayDelayed(delay);
        } else
        {
            source.Play();
        }
    }
}

public class AudioController
{
    /// <summary>
    /// Static method to play a given AudioClipWrapper given an AudioSource.  Will handle playing
    /// the AudioClip with delay depending on the AudioClipControl settings.
    /// </summary>
    public static void PlayAudioClip(AudioSource source, AudioClipWrapper wrapper)
    {
        wrapper.Play(source);
    }
    
    /// <summary>
    /// </summary>
    public static void PlayRandomAudioClip(AudioSource source, List<AudioClipWrapper> wrappers)
    {
        // make sure there are clips to play
        if (wrappers.Count <= 0) return;
        // if we only have one clip then do not bother calculating any random numbers
        else if (wrappers.Count == 1) AudioController.PlayAudioClip(source, wrappers[0]);
        else AudioController.PlayAudioClip(source, wrappers[UnityEngine.Random.Range(0, wrappers.Count)]);
    }
    
    /// <summary>
    /// </summary>
    public static IEnumerator Co_PlayQueuedAudioClips(AudioSource source, params AudioClipWrapper[] clips)
    {
        for(int i = 0; i < clips.Length - 1; i++)
        {
            AudioClipWrapper clip = clips[i];
            
            if (clip.clip != null)
            {
                clip.Play(source);
                float delay = clip.delay + clip.clip.length;
                yield return new WaitForSeconds(delay);
            }
        }
        
        AudioClipWrapper lastClip = clips[clips.Length - 1];
        lastClip.Play(source);
    }
}
