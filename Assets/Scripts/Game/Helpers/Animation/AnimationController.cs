﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class AnimationController
{
    /// <summary>
    /// Definition for the Animation Playback Speed
    /// </summary>
    public static float PlaybackSpeed { get; set; }
    
    /// <summary>
    /// Static Coroutine for handling an On Complete Action to be fired at the end of an animation clip.  Note that 
    /// here we need to calculate the length of the animation clip based off of AnimationController.PlaybackSpeed
    /// since that might not be 1.0f.
    /// </summary>
    public static IEnumerator Co_HandleAnimationComplete(AnimationClip clip, Action OnComplete)
    {
        yield return new WaitForSeconds(clip.length - (PlaybackSpeed - 1.0f) * clip.length);
        OnComplete();
    }
    
    /// <summary>
    /// Static Helper for playing a given Animation Clip (specified by clip name) on an Animation.  If the cancelPrevious
    /// flag is passed through then we will also stop any animations currently playing.
    /// </summary>
    public static void PlayAnimationClip(Animation animation, string clipName, bool cancelPrevious)
    {
        if (cancelPrevious) animation.Stop();
        
        animation[clipName].speed = PlaybackSpeed;
        animation.Play(clipName);
    }
    
    /// <summary>
    /// Static Helper which is a wrapper for AnimationController.PlayAnimationClip to play a random clip from a given
    /// List<string> of animation clip names.
    /// </summary>
    public static void PlayRandomAnimationClip(Animation animation, List<string> clipNames, bool cancelPrevious)
    {
        if (clipNames.Count <= 0) return;
        else if (clipNames.Count == 1) AnimationController.PlayAnimationClip(animation, clipNames[0], cancelPrevious);
        else AnimationController.PlayAnimationClip(animation, clipNames[UnityEngine.Random.Range(0, clipNames.Count)], cancelPrevious);
    }
}
