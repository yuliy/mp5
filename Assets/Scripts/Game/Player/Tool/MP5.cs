﻿using UnityEngine;
using System.Collections.Generic;

public class MP5 : DeformTool
{
    public List<string> FireAnimations = new List<string>();
    
    /// <summary>
    /// TryDeform Post action.  Overridden from the default to change the Animation and Audio behaviour.
    /// </summary>
    protected override void DeformPost()
    {
        AnimationController.PlayRandomAnimationClip(transform.GetComponent<Animation>(), FireAnimations, true);
        AudioController.PlayRandomAudioClip(transform.GetComponent<AudioSource>(), FireSfx);
    }
}
