﻿using UnityEngine;
using System.Collections.Generic;

public enum ToolCode
{
    Glock,
    MP5
}

[RequireComponent (typeof (Animation))]
[RequireComponent (typeof (AudioSource))]
public class DeformTool : MonoBehaviour
{
    #region Constants
    public const string TOOL_TAG = "Tool";
    #endregion
    
    #region ToolCode Definition
    public ToolCode ToolType;
    #endregion
    
    #region Deform Rate
    public float DeformFactor = 0.0f;
    public float Range = 1000.0f;
    public bool Deforming = false;
    #endregion
    
    #region Ammunition
    public int ClipSize = 30;
    public int CurrentClip = 30;
    public bool Reloading = false;
    #endregion
    
    #region Fire Rate
    public bool Auto = false;
    public int FireQuantity = 1;
    public float FireRate = 0.0f;
    public float NextFireTime = 0.0f;
    #endregion
    
    #region Effects
    public GameObject EffectPrefab;
    #endregion
    
    #region Audio
    public AudioClipWrapper DrawSfx;
    public AudioClipWrapper ReloadClipIn;
    public AudioClipWrapper ReloadClipOut;
    public AudioClipWrapper ReloadClipEnd;
    [SerializeField]
    public List<AudioClipWrapper> FireSfx;
    public AudioClipWrapper DryFireSfx;
    #endregion
    
    /// <summary>
    /// MonoBehaviour function for when a MonoBehaviour becomes active.
    /// </summary>
    void Awake()
    {
        transform.name = ToolType.ToString();
        transform.tag = TOOL_TAG;
        
        NextFireTime = Time.time;
    }
    
    /// <summary>
    /// Helper for DeformTool instances to play an animation clip and also DelayFiring based on the length of
    /// the animation clip to play.
    /// </summary>
    protected void PlayAnimationClipDelayFiring(string clip, bool cancelPrevious)
    {
        AnimationController.PlayAnimationClip(transform.GetComponent<Animation>(), clip, cancelPrevious);
        DelayFiring(transform.GetComponent<Animation>().GetClip(clip).length);
    }
    
    /// <summary>
    /// Utility function to determine if we can fire yet.
    /// </summary>
    protected virtual bool CanFire()
    {
        return Time.time > NextFireTime;
    }
    
    /// <summary>
    /// Utility function to determine if the current clip is empty.
    /// </summary>
    protected virtual bool ClipEmpty()
    {
        return CurrentClip <= 0;
    }
    
    /// <summary>
    /// Utility function to update the NextFireTime delay, useful for preventing firing while an
    /// animation is playing.
    /// </summary>
    protected void DelayFiring(float delay)
    {
        NextFireTime = Time.time + delay;
    }
    
    /// <summary>
    /// Activate this Deform Tool
    /// </summary>
    public virtual void Activate()
    {
        transform.gameObject.SetActive(true);
    }
    
    /// <summary>
    /// Deactivate this Deform Tool
    /// </summary>
    public virtual void Deactivate()
    {
        transform.gameObject.SetActive(false);
        Reloading = false; // @todo come back to this to see if we can avoid having to reset the Reloading flag after switching active tools
    }
    
    /// <summary>
    /// </summary>
    public virtual void Draw()
    {
        PlayAnimationClipDelayFiring("Draw", true);
        AudioController.PlayAudioClip(transform.GetComponent<AudioSource>(), DrawSfx);
    }
    
    /// <summary>
    /// </summary>
    public virtual void TryDeform(Transform originTransform)
    {
        if (!CanFire()) return;
        
        if (ClipEmpty())
        {
            // @todo need to clean up this hack
            if (transform.GetComponent<AudioSource>().clip != DryFireSfx.clip) 
                AudioController.PlayAudioClip(transform.GetComponent<AudioSource>(), DryFireSfx);
            else if (!transform.GetComponent<AudioSource>().isPlaying) 
                AudioController.PlayAudioClip(transform.GetComponent<AudioSource>(), DryFireSfx);
            
            return;
        }
        
        Deforming = true;
        NextFireTime = Time.time + FireRate;
        CurrentClip -= FireQuantity;
        
        Ray ray = new Ray(originTransform.position, originTransform.forward);
	  	RaycastHit hit;
        
        if (Physics.Raycast(ray, out hit, Range))
	  	{
            Deform(hit);
        }
        
        DeformPost();
    }
    
    /// <summary>
    /// </summary>
    protected virtual void Deform(RaycastHit hit)
    {
        if (hit.collider.transform.parent != null)
        {
            MeshGenerator generator = hit.collider.transform.parent.GetComponent<MeshGenerator>();
            if (generator != null)
            {
                generator.Deform(
                    new MeshGenerator.DeformOperation(
                        MeshGenerator.DeformOperationCode.REDUCE,
                        DeformFactor,
                        hit.point  
                    )
                );
                
                if (EffectPrefab != null)
                {
                    Instantiate(EffectPrefab, hit.point, transform.rotation);
                }
            }
        }
    }
    
    /// <summary>
    /// </summary>
    protected virtual void DeformPost()
    {
        AnimationController.PlayAnimationClip(transform.GetComponent<Animation>(), "Shoot3", true);
        AudioController.PlayRandomAudioClip(transform.GetComponent<AudioSource>(), FireSfx);
    }
    
    /// <summary>
    /// </summary>
    public virtual void Release()
    {
        Deforming = false;
        if (CurrentClip <= 0)
        {
            Reload();
        }
    }
    
    /// <summary>
    /// </summary>
    public virtual void Reload()
    {
        // When already Reloading do not start over
        if (Reloading) return;
        
        PlayAnimationClipDelayFiring("Reload", true);
        // Queue the audio clips for the various components of the Reload animation
        StartCoroutine(
            AudioController.Co_PlayQueuedAudioClips(
                transform.GetComponent<AudioSource>(),
                ReloadClipOut,
                ReloadClipIn,
                ReloadClipEnd
            )
        );
        
        // Queue up the Reload Complete to Execute at the end of the Animation
        StartCoroutine(
            AnimationController.Co_HandleAnimationComplete(
                transform.GetComponent<Animation>().GetClip("Reload"),
                ReloadComplete
            )
        );
        
        Reloading = true;
    }
    
    /// <summary>
    /// </summary>
    protected virtual void ReloadComplete()
    {
        CurrentClip = ClipSize;
        Reloading = false;
        NextFireTime = 0; // @todo sort of a hack to remove the delay between the end of the reload animation and when you can fire
    }
}
