﻿using UnityEngine;

public class InteractionEvents : MonoBehaviour
{
    /// <summary>
    /// </summary>
    public void Ping()
    {
        print("Ping");
    }
    
    /// <summary>
    /// </summary>
    public void TimePing()
    {
        print("Ping " + Time.time.ToString());
    }
    
    /// <summary>
    /// </summary>
    public void MessagePing(string message)
    {
        print("Ping: " + message);
    }
}
