﻿using UnityEngine;
using UnityEngine.Events;

public class InteractionPoint : MonoBehaviour
{
    #region Public Constants
    public const string INTERACTABLE_TAG = "Interactable";
    #endregion
    
    #region Trigger Settings
    public UnityEvent ToTrigger;
    public float Radius = 2.0f;
    #endregion
    
    #region Trigger Options
    public bool OneShot = false;
    public bool DestroyOnTrigger = false;
    public bool DestroyContainerOnTrigger = false;
    public bool LookAt = true;
    public bool AutoTrigger = false;
    public bool Triggered = false;
    public bool UseOverlay = true;
    public string Message = "Hey there!";
    public AudioClipWrapper TriggerSfx;
    #endregion
    
    /// <summary>
    /// </summary>
    protected bool CanTrigger()
    {
        return (OneShot && !Triggered) || !OneShot;
    }
    
    /// <summary>
    /// </summary>
    public void Trigger()
    {
        if (!CanTrigger()) return;
        
        ToTrigger.Invoke();
        Triggered = true;
        TriggerSfx.Play(GetComponent<AudioSource>());
        
        if (DestroyOnTrigger)
        {
            Destroy(this);
        }
        
        if (DestroyContainerOnTrigger)
        {
            Destroy(gameObject);
        }
    }
    
    /// <summary>
    /// </summary>
    public void HandleInteraction(InteractionController ic)
    {
        if (Vector3.Distance(transform.position, ic.transform.position) < Radius)
        {
            // determine if our InteractionController is facing us (or if we don't care)
            bool facingCheck = (LookAt && ic.CheckFacing(transform)) || !LookAt;
            
            if (facingCheck)
            {
                if (UseOverlay && CanTrigger())
                {
                    ic.UpdateInteractionOverlayText(Message);
                }
                
                if (!AutoTrigger)
                {
                    if (ic.IsInteracting())
                    {
                        Trigger();
                    }
                } else {
                    Trigger();
                }
            }
        }
    }
    
    #region MonoBehaviour Methods
    /// <summary>
    /// </summary>
    void Awake()
    {
        transform.tag = InteractionPoint.INTERACTABLE_TAG;
    }
    #endregion
}
