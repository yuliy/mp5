﻿using UnityEngine;
using UnityEngine.UI;

public class InteractionController : MonoBehaviour
{
    public const string DEFAULT_INTERACTION_OVERLAY_TEXT = "";
    
    public Text InteractionOverlayText;
    public Transform OriginTransform;
    public float Radius = 10.0f;
    
    /// <summary>
    /// Reset the Interaction Overlay Text to the default.
    /// </summary>
    public void ResetInteractionOverlayText()
    {
        InteractionOverlayText.text = InteractionController.DEFAULT_INTERACTION_OVERLAY_TEXT;
    }
    
    /// <summary>
    /// Update the Interaction Overlay Text to display a given message.
    /// </summary>
    public void UpdateInteractionOverlayText(string message)
    {
        InteractionOverlayText.text = message;
    }
    
    /// <summary>
    /// Determine if we are facing a particular point.
    /// </summary>
    public bool CheckFacing(Transform point)
    {
        Ray ray = new Ray(OriginTransform.position, OriginTransform.forward);
	  	RaycastHit hit;
        
        if (Physics.Raycast(ray, out hit, Radius))
	  	{
            if (hit.transform == point)
            {
                return true;
            }
        }
        
        return false;
    }
    
    /// <summary>
    /// Determine if we are currently trying to interact with something.
    /// </summary>
    public bool IsInteracting()
    {
        return Input.GetKeyDown(KeyCode.E);
    }
    
    #region MonoBehaviour Methods
    /// <summary>
    /// </summary>
	void Awake()
    {
	   ResetInteractionOverlayText();
	}
    
    /// <summary>
    /// </summary>
    void Update()
    {
        ResetInteractionOverlayText();
        
        foreach(GameObject interactableTransform in GameObject.FindGameObjectsWithTag(InteractionPoint.INTERACTABLE_TAG))
        {
            foreach(InteractionPoint ip in interactableTransform.GetComponents<InteractionPoint>())
            {
                ip.HandleInteraction(this);
            }
        }
    }
    #endregion
}
