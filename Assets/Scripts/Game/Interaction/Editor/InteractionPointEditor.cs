﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(InteractionPoint))]
public class InteractionPointEditor : Editor
{
    /// <summary>
    /// Overridden to add custom buttons to the Interaction Point Inspector GUI.
    /// </summary>
	public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        
        InteractionPoint ip = (InteractionPoint) target;
        if(GUILayout.Button("Trigger Event"))
        {
            if (Application.isPlaying)
            {
                ip.Trigger();
            } else
            {
                Debug.LogWarning("Cannot trigger event outside of play mode.");
            }
        }
    }
}
