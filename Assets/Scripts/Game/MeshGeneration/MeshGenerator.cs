﻿using UnityEngine;

using System.Collections.Generic;

using Marching;

abstract public class MeshGenerator : MonoBehaviour
{
    public enum DeformOperationCode
    {
        DEFAULT,
        REDUCE,
        INCREASE
    }
    
    public struct DeformOperation
    {
        public DeformOperationCode code;
        public DeformOperationCode inverseCode;
        public float factor;
        public Vector3 impactPoint;
        
        public DeformOperation(DeformOperationCode opCode, float opFactor, Vector3 opImpactPoint)
        {
            code = opCode;
            inverseCode = MeshGenerator.GetInverseDeformOperationCode(code);
            factor = opFactor;
            impactPoint = opImpactPoint;
        }
    }
    
    public Material m_material;
    
    public bool debug = false;
    
    public int fieldSize = 16;
    
    protected GameObject m_mesh;
    
    protected float[,,] voxels;
    
    public GameObject player;
    
    protected Stack<DeformOperation> operations = new Stack<DeformOperation>();
    
    protected bool dirty = true;
    
    # region Static Helpers
    /// <summary>
    /// Retrieve the inverse DeformOperationCode for any given DeformOperationCode.
    /// </summary>
    public static DeformOperationCode GetInverseDeformOperationCode(DeformOperationCode code)
    {
        switch(code)
        {
            case DeformOperationCode.REDUCE:
                return DeformOperationCode.INCREASE;
            case DeformOperationCode.INCREASE:
                return DeformOperationCode.REDUCE;
            default:
                return DeformOperationCode.DEFAULT;
        }
    }
    # endregion

    /// <summary>
    /// Use this for initialization
    /// </summary>
	void Start ()
    {
	   	Init();
	}
    
    /// <summary>
    /// </summary>
    protected virtual void Init()
    {
        // Instantiate the Mesh GameObject
        m_mesh = new GameObject("Mesh");
        m_mesh.AddComponent<MeshFilter>();
    	m_mesh.AddComponent<MeshRenderer>();
        m_mesh.AddComponent<MeshCollider>();
        m_mesh.GetComponent<Renderer>().material = m_material;
        m_mesh.transform.parent = transform; // parent the generated mesh to ourselves (@todo maybe this isn't great)
        
        // Initialize MarchingCubes Settings
        
        //Target is the value that represents the surface of mesh
    	//For example the perlin noise has a range of -1 to 1 so the mid point is were we want the surface to cut through
    	//The target value does not have to be the mid point it can be any value with in the range
    	MarchingCubes.SetTarget(0.0f);
    	
    	//Winding order of triangles use 2,1,0 or 0,1,2
    	MarchingCubes.SetWindingOrder(2, 1, 0);
    	
    	//Set the mode used to create the mesh
    	//Cubes is faster and creates less verts, tetrahedrons is slower and creates more verts but better represents the mesh surface
//     	MarchingCubes.SetModeToCubes();
    	MarchingCubes.SetModeToTetrahedrons();
    }
    
    /// <summary>
    /// Instantiate a Debug Point at a given set of Coordinates.
    /// </summary>
    protected void CreateDebugPoint(int x, int y, int z)
    {
        var l = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        l.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        l.transform.position = new Vector3(x, y, z);
        Destroy(l.GetComponent<Collider>());
    }
    
    /// <summary>
    /// </summary>
    abstract public float GetValue(int x, int y, int z);
    
    /// <summary>
    /// </summary>
    abstract protected void Deform(int x, int y, int z, float factor, DeformOperationCode code = DeformOperationCode.DEFAULT);
    
    /// <summary>
    /// Perform a Deformation on the Generated Mesh given a DeformOperation.
    /// </summary>
    public void Deform(DeformOperation op, bool undo = false) 
    {   
        Deform(
            Mathf.RoundToInt(op.impactPoint.x), 
            Mathf.RoundToInt(op.impactPoint.y), 
            Mathf.RoundToInt(op.impactPoint.z), 
            op.factor, 
            undo ? op.inverseCode : op.code
        );
        
        if (!undo)
        {
            operations.Push(op);
        }
        
        dirty = true;
    }
    
    /// <summary>
    /// Undo the last DeformOperation that has occurred.
    /// </summary>
    public void Undo()
    {
        // Make sure that there are operations in our stack to undo
        if (operations.Count > 0)
        {
            Deform(operations.Pop(), true);
        }
    }
    
    /// <summary>
    /// Retrieve the number of Undo operations available in the Stack
    /// </summary>
    public int UndoCount()
    {
        return operations.Count;
    }
    
    /// <summary>
    /// </summary>
    protected virtual void UpdateMesh()
    {
        // Regenerate our Scalar field by polling every point in the field
        int width = fieldSize;
    	int height = fieldSize;
        int length = fieldSize;
    	
    	for(int x = 0; x < width; x++)
    	{
    		for(int y = 0; y < height; y++)
    		{
    			for(int z = 0; z < length; z++)
    			{
                    voxels[x,y,z] = GetValue(x, y, z);
    			}
    		}
        }
    	
        // Build the Mesh from the Scalar Field
    	Mesh mesh = MarchingCubes.CreateMesh(voxels);
    	
    	//The diffuse shader wants uvs so just fill with a empty array, there not actually used
    	mesh.uv = new Vector2[mesh.vertices.Length];
    	mesh.RecalculateNormals();
        
        // Let Unity try to optimize the Mesh
        mesh.Optimize();
    	
        // Add the Mesh Filter using the Generated Mesh
    	m_mesh.GetComponent<MeshFilter>().mesh = mesh;
        
        // m_mesh.GetComponent<MeshCollider>().sharedMesh = mesh;
	}
	
    /// <summary>
    /// Update is called once per frame
    /// </summary>
	void Update ()
    {
        if (dirty)
        {
            UpdateMesh();
            dirty = false;
        }
	}
    
    void LateUpdate()
    {
        // Add a Collider which is the same as the Generated Mesh
        Mesh mesh = m_mesh.GetComponent<MeshFilter>().mesh;
        m_mesh.GetComponent<MeshCollider>().sharedMesh = mesh;
    }
}
