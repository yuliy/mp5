﻿using UnityEngine;
using UnityEngine.UI;

using System;
using System.Collections.Generic;

public class EditController : MonoBehaviour
{
    #region Origin Point Transform
    public Transform m_Transform;
    #endregion
    
    #region Tool Definitions
    public ToolCode DefaultTool = ToolCode.Glock;
    protected ToolCode CurrentTool;
    public ToolCode LastTool;
    protected Dictionary<ToolCode, GameObject> Tools = new Dictionary<ToolCode, GameObject>();
    protected Func<int, bool> DeformMouseControl;
    protected Action<Transform> OnTry;
    protected Action OnRelease;
    #endregion
    
    #region Animation
    public float AnimationPlaybackSpeed = 1.0f;
    #endregion
    
    #region Canvas Display
    public RectTransform c_Transform;
    public Color CrosshairDefault;
    public Color CrosshairLocked;
    public Text AmmunitionDisplay;
    #endregion
    
    #region Glitch Effects
    public bool EnableGlitch = true;
    public float AnalogGlitchScanlineJitter = 0.0f;
    public float AnalogGlitchVerticalJump = 0.0f;
    public float AnalogGlitchHorizontalShake = 0.0f;
    public float AnalogGlitchColorShift = 0.0f;
    #endregion
    
    /// <summary>
    /// MonoBehaviour function for when a MonoBehaviour becomes active.
    /// </summary>
    void Awake()
    {
        // Fix Animation Playback Speed (@todo should this be moved somewhere else?)
        AnimationController.PlaybackSpeed = AnimationPlaybackSpeed;
        
        /**
         * Garbage way of setting up the active tools for the player, will need to come back to
         * this eventually.
         */
        foreach(var tool in GameObject.FindGameObjectsWithTag(DeformTool.TOOL_TAG))
        {
            if (tool.GetComponent<DeformTool>())
            {
                Tools[tool.GetComponent<DeformTool>().ToolType] = tool;
                
                tool.SetActive(false);
            }
        }
        
        SwitchActiveTool(DefaultTool, true);
    }
    
    /// <summary>
    /// Internal function to switch the currently active tool.  If the force parameter is set to true then the change
    /// will occur even if we are changing to the tool which is already active.
    /// </summary>
    protected void SwitchActiveTool(ToolCode tool, bool force = false)
    {
        if (CurrentTool == tool && !force) return;
        
        // Deactivate the Previous Tool
        Tools[CurrentTool].GetComponent<DeformTool>().Deactivate();
        
        // Setup/Activate the Next Tool
        LastTool = CurrentTool;
        CurrentTool = tool;
        Tools[CurrentTool].GetComponent<DeformTool>().Activate();
        Tools[CurrentTool].GetComponent<DeformTool>().Draw();
        OnTry = Tools[CurrentTool].GetComponent<DeformTool>().TryDeform;
        OnRelease = Tools[CurrentTool].GetComponent<DeformTool>().Release;
        
        if (Tools[CurrentTool].GetComponent<DeformTool>().Auto)
        {
            DeformMouseControl = Input.GetMouseButton;
        } else {
            DeformMouseControl = Input.GetMouseButtonDown;
        }
    }
    
    /// <summary>
	/// Update is called once per frame
    /// </summary>
	void Update ()
    {
        Image crosshairImage = null;
        if (c_Transform != null)
    	{
    		crosshairImage = c_Transform.GetComponent<Image>();
        }
        
        Ray ray = new Ray(m_Transform.position, m_Transform.forward);
	  	RaycastHit hit;
          
        if (Physics.Raycast(ray, out hit, 1000.0f))
	  	{
            // Check for Undo Actions
            if (Input.GetKey(KeyCode.Z) || Input.GetKeyDown(KeyCode.X))
            {
                var hitTransform = hit.collider.transform;
                if (hitTransform.parent != null)
                {
                    MeshGenerator generator = hitTransform.parent.GetComponent<MeshGenerator>();
                    if (generator != null)
                    {
                        if (EnableGlitch && Input.GetKey(KeyCode.Z) && m_Transform.GetComponent<Kino.AnalogGlitch>())
                        {
                            if (generator.UndoCount() > 0)
                            {
                                m_Transform.GetComponent<Kino.AnalogGlitch>().SetParameters(
                                    AnalogGlitchScanlineJitter,
                                    AnalogGlitchVerticalJump,
                                    AnalogGlitchHorizontalShake,
                                    AnalogGlitchColorShift
                                );
                            } else {
                                m_Transform.GetComponent<Kino.AnalogGlitch>().ResetParameters();
                            }
                        }
                        
                        generator.Undo();
                    }
                 }
            }
            
            if (Input.GetKeyUp(KeyCode.Z))
            {
                if (m_Transform.GetComponent<Kino.AnalogGlitch>() != null)
                {
                    m_Transform.GetComponent<Kino.AnalogGlitch>().ResetParameters();
                }
            }
        
            // Check for Crosshair Changes
            if (crosshairImage != null)
        	{
                crosshairImage.color = new Color(CrosshairLocked.r, CrosshairLocked.g, CrosshairLocked.b);
          	}
        } else {
            // Check for Crosshair Changes
            if (crosshairImage != null)
        	{
                crosshairImage.color = new Color(CrosshairDefault.r, CrosshairDefault.g, CrosshairDefault.b);
          	}
        }
        
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SwitchActiveTool(ToolCode.Glock);
        } else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            SwitchActiveTool(ToolCode.MP5);
        }
        
        if (DeformMouseControl(0))
        {
            Cursor.lockState = CursorLockMode.Locked;
			OnTry(m_Transform);
        }
        
        if (Input.GetMouseButtonUp(0))
        {
            OnRelease();
        }
        
        if (Input.GetKeyDown(KeyCode.Q))
        {
            SwitchActiveTool(LastTool);
        }
        
        if (Input.GetKeyDown(KeyCode.R))
        {
            Tools[CurrentTool].GetComponent<DeformTool>().Reload();
        }
        
        AmmunitionDisplay.text = Tools[CurrentTool].GetComponent<DeformTool>().CurrentClip.ToString();
	}
}
