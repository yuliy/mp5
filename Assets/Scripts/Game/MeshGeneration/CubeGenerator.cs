﻿using UnityEngine;

public class CubeGenerator : MeshGenerator
{   
    public Vector3 center = new Vector3(0.0f, 0.0f, 0.0f);
    protected float[,,] radiuses;
 	public float radius = 1.0f;
    
    /// <summary>
    /// </summary>
    protected override void Init()
    {
        int width = fieldSize;
    	int height = fieldSize;
        int length = fieldSize;
        
        voxels = new float[width, height, length];
        radiuses = new float[width, height, length];
    	
    	for (int x = 0; x < width; x++)
    	{
    		for(int y = 0; y < height; y++)
    		{
    			for(int z = 0; z < length; z++)
    			{
                    radiuses[x,y,z] = -1.0f;
                    
                    if (debug)
                    {
                        CreateDebugPoint(x, y, z);
                    }
    			}
    		}
    	}
        
        // Call our Parent Class to finish Initialization tasks
        base.Init();
    }
    
    /// <summary>
    /// </summary>
    public override float GetValue(int x, int y, int z)
    {
        if (
            Mathf.Abs(x - center.x) < radius/2 &&
            Mathf.Abs(y - center.y) < radius/2 &&
            Mathf.Abs(z - center.z) < radius/2 
        )
        {
            return radiuses[x,y,z];
        }
        
        return 1.0f;
    }
    
    /// <summary>
    /// </summary>
    protected override void Deform(int x, int y, int z, float factor, DeformOperationCode code = DeformOperationCode.REDUCE)
    {
        if (
            x >= 0 && x < radiuses.GetLength(0) &&
            y >= 0 && y < radiuses.GetLength(1) &&
            z >= 0 && z < radiuses.GetLength(2)
        ) {
            switch(code)
            {
                case DeformOperationCode.REDUCE:
                    radiuses[x, y, z] += factor;
                    break;
                case DeformOperationCode.INCREASE:
                    radiuses[x, y, z] -= factor;
                    break;
            }
        }
    }
}
