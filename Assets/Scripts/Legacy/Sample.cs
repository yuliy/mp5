﻿using UnityEngine;

using Marching;

public class Sample : MonoBehaviour
{
    public Material m_material;
    
    public bool debug = false;
    
    public int fieldSize = 16;
    
    private GameObject m_mesh;
    
    private float[,,] voxels;
    public static float[,,] radiuses;
    
    public Vector3 center = new Vector3(0.0f, 0.0f, 0.0f);
 	public float radius = 1.0f;
     
    public static void Deform(int x, int y, int z, float factor)
    {
        if (
            x >= 0 && x < Sample.radiuses.GetLength(0) &&
            y >= 0 && y < Sample.radiuses.GetLength(1) &&
            z >= 0 && z < Sample.radiuses.GetLength(2)
        ) {
            Sample.radiuses[x, y, z] += factor;
        }
    }
    
    private float GetValue(float x,float y,float z, int x1, int y1, int z1)
    {
        float distance = Vector3.Distance(new Vector3(x,y,z),center);
        if (distance >= Sample.radiuses[x1,y1,z1])
        {
     	  return 1.0f;
        } else {
     	  return (distance / (Sample.radiuses[x1,y1,z1])) * 2.0f - 1.0f;
        }
    }
    
    private float CubeGetValue(int x, int y, int z)
    {
        if (
            Mathf.Abs(x - center.x) < radius/2 &&
            Mathf.Abs(y - center.y) < radius/2 &&
            Mathf.Abs(z - center.z) < radius/2 
        )
        {
            return Sample.radiuses[x,y,z];
        }
        
        return 1.0f;
    }
    
    private void CreateDebugPoint(int x, int y, int z)
    {
        var l = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        l.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        l.transform.position = new Vector3(x, y, z);
        Destroy(l.GetComponent<Collider>());
    }
    
	// Use this for initialization
	void Start ()
    {
        int width = fieldSize;
    	int height = fieldSize;
        int length = fieldSize;
        
        voxels = new float[width, height, length];
        Sample.radiuses = new float[width, height, length];
    	
    	// Fill voxels with values. Im using perlin noise but any method to create voxels will work
    	for(int x = 0; x < width; x++)
    	{
    		for(int y = 0; y < height; y++)
    		{
    			for(int z = 0; z < length; z++)
    			{
                    Sample.radiuses[x,y,z] = -1.0f;
                    
                    if (debug)
                    {
                        CreateDebugPoint(x, y, z);
                    }
    			}
    		}
    	}
        
        // Instantiate the Mesh GameObject
        m_mesh = new GameObject("Mesh");
        m_mesh.AddComponent<MeshFilter>();
    	m_mesh.AddComponent<MeshRenderer>();
        m_mesh.AddComponent<MeshCollider>();
        m_mesh.GetComponent<Renderer>().material = m_material;
        
        // Initialize MarchingCubes Settings
        
        //Target is the value that represents the surface of mesh
    	//For example the perlin noise has a range of -1 to 1 so the mid point is were we want the surface to cut through
    	//The target value does not have to be the mid point it can be any value with in the range
    	MarchingCubes.SetTarget(0.0f);
    	
    	//Winding order of triangles use 2,1,0 or 0,1,2
    	MarchingCubes.SetWindingOrder(2, 1, 0);
    	
    	//Set the mode used to create the mesh
    	//Cubes is faster and creates less verts, tetrahedrons is slower and creates more verts but better represents the mesh surface
//     	MarchingCubes.SetModeToCubes();
    	MarchingCubes.SetModeToTetrahedrons();
	}
    
    void Update()
    {
        UpdateMesh();
    }
    
    protected void UpdateMesh()
    {
        int width = fieldSize;
    	int height = fieldSize;
        int length = fieldSize;
    	
    	//Fill voxels with values. Im using perlin noise but any method to create voxels will work
    	for(int x = 0; x < width; x++)
    	{
    		for(int y = 0; y < height; y++)
    		{
    			for(int z = 0; z < length; z++)
    			{
                    voxels[x,y,z] = CubeGetValue(x, y, z);
    			}
    		}
        }
    	
    	//The size of voxel array. Be carefull not to make it to large as a mesh in unity can only be made up of 65000 verts
    	
    	Mesh mesh = MarchingCubes.CreateMesh(voxels);
    	
    	//The diffuse shader wants uvs so just fill with a empty array, there not actually used
    	mesh.uv = new Vector2[mesh.vertices.Length];
    	mesh.RecalculateNormals();
        mesh.Optimize();
    	
    	m_mesh.GetComponent<MeshFilter>().mesh = mesh;
        m_mesh.GetComponent<MeshCollider>().sharedMesh = mesh;
	}
}